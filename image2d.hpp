#ifndef IMAGE2D_HPP_INCLUDED
#define IMAGE2D_HPP_INCLUDED
#include <vector>
#include <stdexcept>
#include <iostream>
#include <limits>

struct Image2d {
    int width;
    int height;
    std::vector<uint8_t> data;

    Image2d(int width, int height) :
        width(width), height(height), data(width*height) {}

    uint8_t &operator()(int x, int y) {
        return data[x+y*width];
    }

    uint8_t operator()(int x, int y) const {
        return data[x+y*width];
    }

    static Image2d fromPGM(std::istream &s) {
        std::string str;
        auto comment = [](std::istream &s) -> std::istream &{
            s>>std::ws;
            auto ch=s.get();
            if(ch=='#') while((ch=s.get())!='\n');
            if(ch!=EOF) s.putback(ch);
            return s;
        };
        s>>str>>comment;
        int width=-1, height=-1, maxval=-1;
        s>>width>>comment;
        s>>height>>comment;
        s>>maxval>>comment;
        if(str!="P5" || width<0 || height<0 || maxval<=0 || maxval>255) {
            throw std::runtime_error("Bad PGM header");
        }
        s>>std::ws;
        Image2d ret(width, height);
        s.read((char *)&ret.data[0], width*height);
        if(!s) throw std::runtime_error("Couldn't read PGM");
        return ret;
    }

    void savePGM(std::ostream &os) {
        os<<"P5\n"<<width<<" "<<height<<"\n"<<255<<"\n";
        os.write((char *)&data[0], width*height);
    }
};

#endif
