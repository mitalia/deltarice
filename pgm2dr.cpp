#include <iostream>
#include <string>
#include <fstream>
#include "image2d.hpp"
#include "bitstream.hpp"

void mkrice(unsigned x, OBitStream &out, unsigned k) {
    out.writeZeroes(x>>k);
    out.writeBit(true);
    out.writeInt(x,k);
}

unsigned ricecount(unsigned x, unsigned k) {
    return (x>>k)+k;
}

unsigned sdelta(int a, int b) {
    int delta = a-b;
    if(delta<0) return ((-delta)<<1) | 1;
    return delta<<1;
}

int verbose = 0;

void drEncode(const Image2d &img, OBitStream &out)
{
    out.aWriteStr("DRFg");
    out.aWriteWord(1);
    out.aWriteWord(img.width);
    out.aWriteWord(img.height);
    for(int y=0; y<img.height; ++y) {
        unsigned ricesz[16]={0};
        for(int x=0; x<img.width; ++x) {
            int px=img(x, y);
            int vpx = y==0?0:img(x,y-1);
            int hpx = x==0?0:img(x-1,y);
            for(int i=0; i<8; ++i) {
                ricesz[i]  +=ricecount(sdelta(px, hpx), i+1);
                ricesz[i+8]+=ricecount(sdelta(px, vpx), i+1);
            }
        }
        int best=0;
        for(int i=0; i<16; ++i) {
            if(ricesz[i]<ricesz[best]) best=i;
        }
        if(verbose > 1) {
            std::cerr<<"Row "<<y<<" - best: "<<"vh"[best<8]<<((best&7)+1)<<" ("<<ricesz[best]<<")\n";
        }
        out.writeInt(best, 5);
        if(best<8) {
            for(int x=0; x<img.width; ++x) {
                int px=img(x, y);
                int hpx = x==0?0:img(x-1,y);
                mkrice(sdelta(px, hpx), out, (best&7)+1);
            }
        } else {
            for(int x=0; x<img.width; ++x) {
                int px=img(x, y);
                int vpx = y==0?0:img(x,y-1);
                mkrice(sdelta(px, vpx), out, (best&7)+1);
            }
        }
    }
    out.flush();
}

void drDecode(Image2d &img, IBitStream &in) {
   if(in.aReadStr(4)!="DRFg") throw std::runtime_error("Invalid file signature");
   if(in.aReadWord()!=1) throw std::runtime_error("Unsupported file version");
   img.width = in.aReadWord();
   img.height = in.aReadWord();
   img.data.resize(img.width*img.height);
   for(int y=0; y<img.height; ++y) {
       uint32_t rowhdr = in.readInt(5);
       unsigned ricesz = (rowhdr & 7) + 1;
       bool vref = rowhdr&8;
       for(int x=0; x<img.width; ++x) {
           uint8_t ref=0;
           if(vref) {
               if(y>0) ref = img(x, y-1);
           } else {
               if(x>0) ref = img(x-1, y);
           }
           unsigned upper = in.readZeroes();
           int delta = in.readInt(ricesz);
           delta += upper<<ricesz;
           if(delta & 1) delta = -(delta>>1);
           else          delta =   delta>>1;
           img(x, y) = ref+delta;
       }
   }
}

int main(int argc, char *argv[]) {
    bool decompress = false;
    for(int i=1; i<argc; ++i) {
        auto invalid = [&]() {
            std::cerr<<"Invalid argument: "<<argv[i]<<"\n";
            std::exit(1);
        };
        char *rp=argv[i];
        if(*rp++!='-') invalid();
        if(*rp=='d' && !rp[1]) {
            decompress = true;
            continue;
        }
        while(*rp++=='v') ++verbose;
        --rp;
        if(*rp) invalid();
    }
    if(decompress) {
        IBitStream in;
        in.in = std::vector<uint8_t>(
                    std::istreambuf_iterator<char>(std::cin), {});
        Image2d img(0, 0);
        drDecode(img, in);
        img.savePGM(std::cout);
    } else {
        Image2d img = Image2d::fromPGM(std::cin);
        OBitStream out;
        drEncode(img, out);
        if(verbose > 0) {
            std::cerr<<"Size: "<<out.output.size()<<"\n";
        }
        std::cout.write((char *)&out.output.front(), out.output.size());
    }
    return 0;
}
