#ifndef BITSTREAM_HPP
#define BITSTREAM_HPP
#include <vector>
#include <stdint.h>
#include <string>

//#define BITSTREAM_FAILSAFE

struct OBitStream {
    std::vector<uint8_t> output;
    unsigned pos=0;
    uint8_t working=0;

    void writeBit(bool bit) {
        if(bit) {
            working|=1<<pos;
        }
        pos++;
        if(pos==8) {
            flushByte();
        }
    }

    void writeZeroes(unsigned count) {
#ifdef BITSTREAM_FAILSAFE
        for(unsigned i=0; i<count; ++i) {
            writeBit(0);
        }
#else
        unsigned available = 8-pos;
        if(count<available) {
            pos+=count;
            return;
        }
        flushByte();
        count-=available;
        while(count>=8) {
            output.push_back(0);
            count-=8;
        }
        pos=count;
#endif
    }

    void writeInt(uint32_t n, unsigned width) {
#ifdef BITSTREAM_FAILSAFE
        for(unsigned int i=0; i<width; ++i) {
            writeBit(n & (1<<i));
        }
#else
        unsigned available = 8-pos;
        working|=n<<pos;
        if(width<available) {
            pos+=width;
            working&=(1<<pos)-1;
            return;
        }
        output.push_back(working);
        n>>=available;
        width-=available;
        while(width>=8) {
            output.push_back(n);
            n>>=8;
            width-=8;
        }
        working=n & ((1<<width)-1);
        pos=width;
#endif
    }

    void flush() {
        if(pos!=0) flushByte();
    }

    void aWriteWord(uint16_t word) {
        flush();
        output.push_back(word&0xff);
        output.push_back(word>>8);
    }

    void aWriteStr(const char *str) {
        flush();
        while(*str) {
            output.push_back(*str);
            ++str;
        }
    }

private:
    void flushByte() {
        output.push_back(working);
        working=0;
        pos=0;
    }

};

struct IBitStream {
    std::vector<uint8_t> in;
    unsigned rp=0;
    unsigned pos=8;
    uint8_t working;

    bool readBit() {
        prepareByte();
        bool ret = working & (1<<pos);
        ++pos;
        return ret;
    }

    unsigned readZeroes() {
#ifdef BITSTREAM_FAILSAFE
        unsigned ret=0;
        while(!readBit()) ret++;
        return ret;
#else
        prepareByte();
        unsigned ret=0;
        //working &= ~((1<<pos)-1);
        working >>= pos;
        working <<= pos;
        if(working) {
            while(!(working&(1<<pos++))) ret++;
            return ret;
        }
        ret+=8-pos;
        while(!(working=in[rp++])) ret+=8;
        pos=0;
        while(!(working&(1<<pos++))) ret++;
        return ret;
#endif
    }

    uint32_t readInt(unsigned width) {
#ifdef BITSTREAM_FAILSAFE
        uint32_t ret=0;
        for(unsigned i=0; i<width; ++i) {
            ret |= readBit()<<i;
        }
        return ret;
#else
        prepareByte();
        uint32_t ret=0;
        unsigned available = 8-pos;
        ret = working >> pos;
        if(width<available) {
            pos+=width;
            return ret & ((uint32_t(1)<<width)-1);
        }
        working=in[rp++];
        unsigned read = available;
        width-=available;
        while(width>8) {
            ret|=working<<read;
            working=in[rp++];
            read+=8;
            width-=8;
        }
        ret|=(working&((uint32_t(1)<<width)-1))<<read;
        pos=width;
        return ret;
#endif
    }

    uint16_t aReadWord() {
        uint16_t ret = uint16_t(in[rp+1])<<8 | in[rp];
        rp+=2;
        pos=8;
        return ret;
    }

    std::string aReadStr(unsigned sz) {
        std::string ret((char *)&in[rp], (char *)&in[rp+sz]);
        rp+=sz; pos=8;
        return ret;
    }

private:
    void prepareByte() {
        if(pos==8) getNext();
    }

    void getNext() {
        working = in[rp];
        rp++;
        pos=0;
    }
};

#endif // BITSTREAM_HPP

