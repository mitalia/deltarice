# DeltaRice #
## a simplicistic image compression algorithm ##

DeltaRice is an attempt to create a fast but decent lossless image compression algorithm; DR should:

- give decent results - i.e. it should at least beat a plain gzip of the original uncompressed bitmap;
- be reasonably fast, to compress on the fly images captured by ARM-based cameras;
- be reasonably simple to implement from scratch;
- have no dependencies besides the C++ standard library.

The current approach is based on encoding deltas of adjacent (in vertical or horizontal) pixels with Rice encoding of various lengths, taking the best result for each scanline.
